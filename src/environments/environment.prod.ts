export const environment = {
  name: 'production',
  production: true,
  fxApiClientUrl: 'https://customers-api.floraxchange.nl',
};
