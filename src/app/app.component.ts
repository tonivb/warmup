import { Component } from '@angular/core';
import { User } from 'oidc-client';
import { map } from 'rxjs';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Warm-up';
  user: User;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.authService.user$.subscribe((user: User) => {
      this.user = user;
    });
  }

  login(): void {
    this.authService.startAuthentication();
  }

  logout(): void {
    this.authService.logout();
  }
}
