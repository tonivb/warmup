import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ItemDto } from 'src/app/Clients/FxApiClient';
import { ItemService } from 'src/app/services/item.service';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {
  moment = moment;
  displayedColumns: string[] = ['availability', 'id', 'name', 'modified'];
  initialItems?: ItemDto[] = [];
  items?: ItemDto[] = [];
  form: FormGroup;

  constructor(private itemService: ItemService) {
    this.form = new FormGroup({
      text: new FormControl(''),
      availability: new FormControl(undefined),
    });
    this.form.get('text').valueChanges.subscribe(() => this.filter());
    this.form.get('availability').valueChanges.subscribe(() => this.filter());
  }

  ngOnInit(): void {
    this.getItems();
  }

  getItems(): void {
    this.itemService.getItems()
      .subscribe(items => { this.initialItems = _.orderBy(items, ['lastModified', 'name',], ['desc', 'asc']); this.items = _.orderBy(items, ['lastModified', 'name',], ['desc', 'asc']) });
  }

  filter() {
    const availability: boolean = this.form.get('availability').value;
    let textFilter: string = this.form.get('text').value;

    let filterdItems = this.initialItems;

    if (availability != undefined) {
      filterdItems = _.filter(filterdItems, i => i.currentlyAvailable === availability);
    }

    if (textFilter) {
      textFilter = textFilter.toLowerCase();
      filterdItems = _.filter(filterdItems, i => i.code.toLowerCase().includes(textFilter) || i.name.toLowerCase().includes(textFilter) || i.id.toString().includes(textFilter));
    }

    this.items = filterdItems;
  }
}
