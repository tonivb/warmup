import { Component, OnInit } from '@angular/core';

import { User } from 'oidc-client';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
  companyCode = 0;
  user: User;

  constructor(
    private authService: AuthService) { }

  ngOnInit(): void {
    this.authService.user$.subscribe((user: User) => {
      this.user = user;
    });
  }
}
