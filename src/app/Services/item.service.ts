import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FxApiClient, ItemDto } from '../Clients/FxApiClient';
import { AuthService } from './auth.service';


@Injectable({
  providedIn: 'root'
})
export class ItemService {

  constructor(private client: FxApiClient, private authService: AuthService) { }

  getItems(): Observable<ItemDto[]> {
    const items = this.client.itemsAll(null, null);
    return items;
  }

  getItem(id: number): Observable<ItemDto> {
    const item = this.client.items(id);
    return item;
  }
}
