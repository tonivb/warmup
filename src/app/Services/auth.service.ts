import { User, UserManager, UserManagerSettings, WebStorageStateStore } from 'oidc-client';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

export class AuthService {
  private userManager: UserManager;

  private user: User;
  private currentUserObservable$: Observable<User>;
  private userObservable$: Observable<User>;
  private currentUserSubject: BehaviorSubject<User> = new BehaviorSubject<User>(undefined);

  public isLoggedIn = false;
  public activeUserChanged: Subject<User> = new Subject<User>();


  constructor() {
    const userManagerSettings: UserManagerSettings = {
      authority: 'https://auth.floraxchange.nl',
      client_id: 'b1db9c7b-811e-4998-9de9-3695d8246920',
      redirect_uri: 'http://localhost:3000/signin-callback',
      post_logout_redirect_uri: 'http://localhost:3000',
      response_type: 'token',
      scope: 'catalog:read',
      userStore: new WebStorageStateStore({ store: localStorage })
    }

    this.userManager = new UserManager(userManagerSettings);
    this.isLoggedIn = false;

    this.userManager.getUser().then((user: User) => {
      console.log('setting user');
      if (user && user.expired) {
        this.handleUserLogin(user);
      } else {
        this.handleUserLogin(undefined);
      }
    });
  }

  get user$(): Observable<User | null> {
    console.log('Fetching user');
    if (!this.userObservable$) {
      this.userObservable$ = this.activeUserChanged.asObservable();
    }
    return this.userObservable$;
  }

  get activeUser$(): Observable<User | null> {
    console.log('Fetching user for activeuser');
    if (!this.currentUserObservable$) {
      this.currentUserObservable$ = this.currentUserSubject.asObservable();
    }
    return this.currentUserObservable$;
  }

  getAuthorizationHeaderValue(): string {
    console.log(this.user);
    return `${this.user.token_type} ${this.user.access_token}`;
  }

  startAuthentication(): Promise<void> {
    return this.userManager.signinRedirect();
  }

  completeAuthentication(): Promise<void> {
    return this.userManager.signinRedirectCallback().then(user => {
      this.handleUserLogin(user);
    });
  }

  logout(): Promise<void> {
    return this.userManager.signoutRedirect().then(() => {
      this.removeAuthenticatedUser();
    });
  }

  public removeAuthenticatedUser(): Promise<void> {
    return this.userManager
      .removeUser()
      .then(() => {
        this.handleUserLogin(undefined);
      });
  }

  private handleUserLogin(user: User): void {
    this.user = user;
    if (user) {
      this.isLoggedIn = true;
    } else {
      this.isLoggedIn = false;
    }
    this.activeUserChanged.next(this.user);
    this.currentUserSubject.next(this.user);
  }
}