import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-call-api',
  templateUrl: './call-api.component.html',
  styleUrls: ['./call-api.component.scss']
})
export class CallApiComponent implements OnInit {
  response: Object;
  constructor(private http: HttpClient, private authService: AuthService) { }

  ngOnInit(): void {
    let headers = new HttpHeaders({ 'Authorization': this.authService.getAuthorizationHeaderValue() })
    console.log(headers);
    this.http.get('https://customers-api.floraxchange.nl/Items', { headers: headers })
      .subscribe(response => this.response = response);
  }
}
