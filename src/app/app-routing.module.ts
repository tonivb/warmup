import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SigninCallbackComponent } from './components/auth-callback/signin-callback.component';
import { CallApiComponent } from './components/call-api/call-api.component';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { DetailComponent } from './modules/Items/detail/detail.component';
import { ItemsComponent } from './modules/Items/items.component';


import { AuthGuardService } from './services/auth-guard.service';

export const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'items', component: ItemsComponent, canActivate: [AuthGuardService] },
  { path: 'items/detail/:id', component: DetailComponent, canActivate: [AuthGuardService] },
  { path: 'call-api', component: CallApiComponent, canActivate: [AuthGuardService] },
  { path: 'signin-callback', component: SigninCallbackComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }