import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './material.module';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FxApiClient, FX_API_BASE_URL } from './Clients/FxApiClient';
import { environment } from 'src/environments/environment';
import { AuthGuardService } from './services/auth-guard.service';
import { AuthService } from './services/auth.service';

import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { ItemsComponent } from './modules/Items/items.component';
import { DetailComponent } from './modules/Items/detail/detail.component';
import { CallApiComponent } from './components/call-api/call-api.component';
import { SigninCallbackComponent } from './components/auth-callback/signin-callback.component';
import { TokenInterceptor } from './Clients/TokenInterceptor';

@NgModule({
  declarations: [
    AppComponent,
    SigninCallbackComponent,
    DashboardComponent,
    ItemsComponent,
    DetailComponent,
    CallApiComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [
    AuthService,
    FxApiClient,
    AuthGuardService,
    {
      provide: FX_API_BASE_URL,
      useFactory: () => { return `${environment.fxApiClientUrl}` },
    },
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
